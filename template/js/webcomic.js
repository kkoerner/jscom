var presentationState = {
    chapter : "c01",
    currentEpisode : "e01",
    eTitle : "",
    stripList : [],
    mapList : [],
    alertList : [],
    refList : [],
    currentStrip : 0,

    setChapter : function(chapter){
        this.chapter = chapter;
    },

    setCurrentEpisode : function(currentEpisode){
        this.currentEpisode = currentEpisode;
    },

    setCurrentStrip : function(currentStrip){
        this.currentStrip = currentStrip;
    },

    setETitle : function(eTitle){
        this.eTitle = eTitle;
    },

    getCurrentStrip : function(){
        return this.stripList[this.currentStrip];
    },

    getStripAlerts : function(){
        return this.alertList[this.currentStrip];
    },

    getStripMap : function(){
        return this.mapList[this.currentStrip];
    },

    getStripRefs : function(){
        return this.refList[this.currentStrip];
    },

    nextStrip : function(){
        if(this.currentStrip < this.stripList.length-1){
            this.currentStrip++;
        }else{
            this.onEpisodeEnd(); // see xmlloader.js
        }
        this.store();

    },

    previousStrip : function(){
        if(this.currentStrip > 0){
            this.currentStrip--;
        }else{
            this.onBeforeEpisode();
        }
        this.store();

    },

    reset : function(){
        this.stripList = [];
        this.mapList = [];
        this.alertList = [];
        this.refList = [];
        this.currentStrip = 0;
    },

    onEpisodeEnd : function(){
        let nextEpisode = nextEpisodeInChapter(this.chapter, this.currentEpisode);
        if(nextEpisode != null){
            setStrips(this.chapter, nextEpisode, 0);
        }else{
            let nextChapter = getNextChapter(this.chapter);
            if(nextChapter != null){
                setStrips(nextChapter, "e01", 0);
            }else{
                console.log("Last chapter");
            }
        }
    },

    onBeforeEpisode : function(){
        let previousEpisode = previousEpisodeInChapter(this.currentEpisode);
        if(previousEpisode != null){            
            setStrips(this.chapter, previousEpisode, this.stripList.length-1);
            this.currentStrip = this.stripList.length-1;
        }else{
            let previousChapter = getPreviousChapter(this.chapter);
            if(previousChapter != null){
                previousEpisode = lastEpisodeInChapter(previousChapter);
                setStrips(previousChapter, previousEpisode, this.stripList.length-1);
                this.currentStrip = this.stripList.length - 1;
            }else{
                console.log("First Strip");
            }
        }
    },

    store : function(){
        if(this.readCookieData("cookieAccept") == "true"){
            let expires = getExpireTime();
            document.cookie = `chapter=${this.chapter}; expires=${expires}; path=/`;
            document.cookie = `episode=${this.currentEpisode}; expires=${expires}; path=/`;
            document.cookie = `strip=${this.currentStrip}; expires=${expires}; path=/`;
        }
    },

    readCookieData : function(name){
        let cookie = document.cookie;
        let result = null;
        if(cookie != null && cookie != ""){
            let entries = cookie.split(";");
            for(let i=0; i<entries.length; i++){
                if(entries[i].includes(name)){
                    result = entries[i].split("=")[1];
                }
            }
        }
        return result;
    },

    visibleTitle : function(){

        let chapter = this.chapter.substring(1);
        return `Kapitel ${chapter}: <br /> ${this.eTitle}`;
    //        let episode = this.currentEpisode.substring(1);
    //        return `Episode ${episode}: ${this.eTitle}`;
    },

    atFirstStrip(){
        return atFirstStrip(this.chapter, this.currentEpisode, this.currentStrip);
    },

    atLastStrip(){
        return atLastStrip(this.chapter, this.currentEpisode, this.currentStrip);
    }

}

function toggleImage(node){
    if(node.src.includes("disabled")){
        disableArrow();
    }else{
        enableArrow();
    }
}

function disableArrow(name, node){
    node.src = `assets/${name}_disabled.png`;
}

function enableArrow(name, node){
    node.src = `assets/${name}.png`;
}

function resetImgMap(){
    document.getElementById("imgmap").innerHTML = '';
}

function resetReferences(){
    document.getElementById("references").innerHTML = '';
}

function addMapItem(mapItem){
    document.getElementById("imgmap").appendChild(mapItem);
}

function addReference(reference){
    document.getElementById("references").appendChild(reference);
}

function setMapData(){
    resetImgMap();
    setLinkAreas();
    setPopupAreas();

    resetReferences();
    setReferences();
}

function setReferences(){
    let stripReferences = presentationState.getStripRefs();
    for(let i=0; i<stripReferences.length; i++){
        addReference(stripReferences[i]);
    }
}

function setLinkAreas(){
    let mapItems = presentationState.getStripMap();
    
    for(let i=0; i<mapItems.length; i++){
        addMapItem(mapItems[i]);
    }
}

function setPopupAreas(){
    let popupAreas = presentationState.getStripAlerts();
    for(let i=0; i<popupAreas.length; i++){
        addMapItem(popupAreas[i]);
    }
}

function showStrip(){
    document.getElementById("stripContainer").src = document.getElementById(presentationState.getCurrentStrip()).src;
    document.getElementById("eTitle").innerHTML = presentationState.visibleTitle();  
    
    setMapData();
    addRwdImageMaps();

    setLeftArrowIcon();
    setRightArrowIcon();
}

function setLeftArrowIcon(){
    if(presentationState.atFirstStrip()){
        disableArrow("leftarrow", document.getElementById("leftArrow"));        
    }else{
        enableArrow("leftarrow", document.getElementById("leftArrow"));
    }
}

function setRightArrowIcon(){
    if(presentationState.atLastStrip()){
        disableArrow("rightarrow", document.getElementById("rightArrow"));
    }else{
        enableArrow("rightarrow", document.getElementById("rightArrow"));
    }
}

function leftSwipe(){
    presentationState.nextStrip();
    showStrip();
}

function rightSwipe(){
    presentationState.previousStrip();
    showStrip();
}

function showContent(event){
    let content = event.target.getAttribute("data-content");
    document.body.classList.add("noscroll");
    document.getElementById("infobox").innerHTML = content;
    document.getElementById("overlay").style.display = "block";
    document.getElementById("overlay").addEventListener("click", hideContent);
    event.preventDefault();
    return false;
}

function hideContent(){
    document.getElementById("overlay").style.display = "none";
    document.getElementById("overlay").removeEventListener("click", hideContent);
    document.body.classList.remove("noscroll");
}

function addRwdImageMaps(){
    $('img[usemap]').rwdImageMaps();
}

function onStart(){
    addRwdImageMaps();
    document.getElementById("rightArrow").addEventListener("click", leftSwipe);
    document.getElementById("leftArrow").addEventListener("click", rightSwipe);
    window.addEventListener("keyup", keyUp);
    hideContent();
}

function keyUp(event){
    if(event.keyCode == "39"){
        leftSwipe();
    }else if(event.keyCode == "37"){
        rightSwipe();
    }
}

function getExpireTime(){
    let expires = new Date();
    expires.setTime(expires.getTime() + 14*24*60*60*1000);
    return expires.toUTCString();
}

window.addEventListener("load", onStart);