let doc;

function start(){
    loadXML();
}

function loadXML(){
    var Connect = new XMLHttpRequest();
    Connect.open("GET", "strips/config.xml", true);
    Connect.onreadystatechange = (function(){
        if (Connect.readyState == 4 && Connect.status == 200){
            onLoaded(this.responseText);
        }
    });
    Connect.setRequestHeader("Content-Type", "text/xml");
    Connect.send(null);
}

function cookieData(){
    return JSON.parse(document.cookie);
}

function fromURL(data){

    // Get the current URL
    let href = window.location.href;
    var url = new URL(href);

    // Read the param from it
    let paramData = null;

    if(data == "chapter"){
        paramData = url.searchParams.get("c");
    }else if(data == "episode"){
        paramData = url.searchParams.get("e");
    }else if(data == "strip"){
        paramData = url.searchParams.get("s");
    }

    return paramData;
}

function onLoaded(xml){
    // Get chapterToShow, Get episodeToShow, Get StripToShow - from cookies - 
    let chapter = fromURL("chapter");
    if(chapter == null) chapter = presentationState.readCookieData("chapter"); 
    if(chapter == null) chapter = "c01"; // fallback - just begin at chapter1, episode1 
    
    let episode = fromURL("episode");
    if(episode == null) episode = presentationState.readCookieData("episode");
    if(episode == null) episode = "e01"; // Fallback

    let strip = fromURL("strip");
    if(strip == null) strip = presentationState.readCookieData("strip");
    if(strip == null) strip = 0; // Fallback

    // In any case: Load all Strips from the current episode
    let parser = new DOMParser();
    doc = parser.parseFromString(xml,"text/xml");

    setStrips(chapter, episode, strip);
    showStrip();
}

function setStrips(chapter, episode, strip){
    presentationState.reset();

    let chapterNode = doc.getElementById(chapter);
    let episodeNode = chapterNode.querySelectorAll(`[episode-data="${episode}"]`)[0];
    let eTitle = episodeNode.getAttribute("title");
    let strips = episodeNode.children;
    
    for(let i=0; i<strips.length; i++){
        let id = strips[i].getAttribute("strip-data");
        let stripFileName = `${chapter}_${episode}_${id}`; 
        preloadImg(stripFileName);
        loadMapData(strips[i]);
        loadAlertData(strips[i]);
        loadRefData(strips[i]);
    } 

    presentationState.setChapter(chapter);
    presentationState.setCurrentEpisode(episode);
    presentationState.setETitle(eTitle);
    presentationState.setCurrentStrip(parseInt(strip));
}

function preloadImg(id){
    let imgNode = document.createElement("img");
    imgNode.src = `strips/${id}.png`;
    imgNode.id = `strip${id}`;

    document.getElementById("hiddenimgs").appendChild(imgNode);

    presentationState.stripList.push(imgNode.id);
}

function loadRefData(stripNode){
    let refDataNodes = stripNode.getElementsByTagName("references");

    let refData = [];

    for(let i=0; i<refDataNodes.length; i++){
        let refNode = createRefNode(refDataNodes[i]);
        refData.push(refNode);
    }

    presentationState.refList.push(refData);
}

function loadMapData(stripNode){
    let mapItemNodes = stripNode.getElementsByTagName("area");
    let mapItems = [];

    for(let i=0; i<mapItemNodes.length; i++){
        let areaNode = createAreaNode(mapItemNodes[i]);
        mapItems.push(areaNode);
    }

    presentationState.mapList.push(mapItems);
}

function loadAlertData(stripNode){
    let alertItemNodes = stripNode.getElementsByTagName("popup");
    let popupItems = [];

    for(let i=0; i<alertItemNodes.length; i++){
        let alertNode = createAlertNode(alertItemNodes[i]);
        popupItems.push(alertNode);
    }

    presentationState.alertList.push(popupItems);
}

function createAlertNode(alertItemNode){
    let shape = alertItemNode.getAttribute("shape");
    let coords = alertItemNode.getAttribute("coords");
    
    let areaNode = document.createElement("area");
    areaNode.setAttribute("shape", shape);
    areaNode.setAttribute("coords", coords);
    areaNode.setAttribute("href", "#");
    areaNode.setAttribute("data-content", alertItemNode.innerHTML);
    
    areaNode.addEventListener("click", showContent)
    return areaNode;
}

function createAreaNode(mapItemNode){
    let shape = mapItemNode.getAttribute("shape");
    let coords = mapItemNode.getAttribute("coords");
    let href = mapItemNode.getAttribute("href");
    let target = mapItemNode.getAttribute("target");
    let areaNode = document.createElement("area");
    areaNode.setAttribute("shape", shape);
    areaNode.setAttribute("coords", coords);
    areaNode.setAttribute("href", href);
    areaNode.setAttribute("target", target);
    return areaNode;
}

function createRefNode(refData){
    let refNode = document.createElement("span");
    refNode.innerHTML = refData.innerHTML;
    return refNode;
}

function nextEpisodeInChapter(chapter, currentEpisode){
    
    let chapterNode = doc.getElementById(chapter);
    let episodeNodes = chapterNode.querySelectorAll(`[episode-data]`);

    let nextEpisode = null;
    let pattern = /\d+/;
    
    let episodeNr = parseInt(currentEpisode.match(pattern));
    if(episodeNr < episodeNodes.length){
        nextEpisode = "e" + ("00" + (episodeNr+1)).slice(-2);
    }
    
    return nextEpisode;
}

function previousEpisodeInChapter(currentEpisode){
    let previousEpisode = null;
    
    let pattern = /\d+/;
    let episodeNr = parseInt(currentEpisode.match(pattern));
    if(episodeNr > 1){
        previousEpisode = "e" + ("00" + (episodeNr-1)).slice(-2);
    }
    
    return previousEpisode;
}

function getNextChapter(currentChapter){
    let pattern = /\d+/;
    let chapterNr = parseInt(currentChapter.match(pattern));
    let nextChapterID = "c" + ("00" + (chapterNr+1)).slice(-2);
    
    if(doc.getElementById(nextChapterID) == null){
        nextChapterID = null;
    }
    
    return nextChapterID;
}

function getPreviousChapter(currentChapter){
    let pattern = /\d+/;
    let chapterNr = parseInt(currentChapter.match(pattern));
    let nextChapterID = "c" + ("00" + (chapterNr-1)).slice(-2);
    
    if(doc.getElementById(nextChapterID) == null){
        nextChapterID = null;
    }
    
    return nextChapterID;
}

function lastEpisodeInChapter(chapter){
    let chapterNode = doc.getElementById(chapter);
    let episodeNodes = chapterNode.querySelectorAll(`[episode-data]`);

    return "e" + ("00" + episodeNodes.length).slice(-2);
}

function atLastStrip(chapter, episode, strip){
    let chapters = doc.getElementsByTagName("chapter");
    let lastChapter = chapters[chapters.length - 1];
    
    let episodesInLastChapter = lastChapter.getElementsByTagName("episode");
    let lastEpisode = episodesInLastChapter[episodesInLastChapter.length - 1];
    
    let lastStrip = lastEpisode.getElementsByTagName("strip").length - 1;
    
    let reachedLastStrip = chapter == lastChapter.getAttribute('id') &&
                           episode == lastEpisode.getAttribute('episode-data') &&
                           strip == lastStrip;

    return reachedLastStrip;
}

function atFirstStrip(chapter, episode, strip){
    return chapter == "c01" && episode == "e01" && strip == 0;
}

window.addEventListener("load", start);