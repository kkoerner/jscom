// Touch Input Handling
let xDown = null;                                                        
let yDown = null;

let xUp = null;
let yUp = null;

const deltaDiff = 100;

function getTouches(evt) {
  return evt.touches ||             // browser API
         evt.originalEvent.touches; // jQuery
}                                                     

function handleTouchStart(evt) {
    const firstTouch = getTouches(evt)[0];                                      
    xDown = firstTouch.clientX;              
};                                                

function handleTouchMove(evt) {
    if ( ! xDown ) {
        return;
    }
    
    xUp = evt.touches[0].clientX;                                    
};

function evalTouchMove(){

    if(xUp == null){
        return;
    }

    var xDiff = xDown - xUp;
    
    if (Math.abs( xDiff ) > deltaDiff) {
        if ( xDiff > 0 ) {
            leftSwipe(); // Located in webcomic.js
        } else {
            rightSwipe(); // Located in webcomic.js
        }                       
    }
    
    xDown = null;
    xUp = null;
}

document.addEventListener('touchstart', handleTouchStart, false);        
document.addEventListener('touchmove', handleTouchMove, false);
document.addEventListener('touchend', evalTouchMove, false);